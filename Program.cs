﻿using System;
using System.IO;
using ProtoBuf;

namespace pbtest
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            var person = new Person {
                Id = 12345,
                Name = "Fred",
                Address = new Address {
                    Line1 = "Flat 1",
                    Line2 = "The Meadows"}
            };

            using (var file = File.Create("person.bin")) {
                Serializer.Serialize(file, person);
            }

            Person newPerson;
            using (var file = File.OpenRead("person.bin")) {
                newPerson = Serializer.Deserialize<Person>(file);
            }

            Console.WriteLine(newPerson);

            string proto = Serializer.GetProto<Person>();

            Console.WriteLine(proto);
            Console.ReadKey();
        }
    }
}