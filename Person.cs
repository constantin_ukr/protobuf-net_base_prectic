﻿using ProtoBuf;

namespace pbtest
{
    [ProtoContract]
    public class Person
    {
        [ProtoMember(1, IsRequired = true)]
        public int Id { get; set; }

        [ProtoMember(2, IsRequired = false)]
        public string Name { get; set; }

        [ProtoMember(3)]
        public Address Address {get; set;}
    }
}